 /* global $ */
if (!process.env.PORT)
  process.env.PORT = 8081;ReferenceError: document is not defined
var express = require('express');
var bodyParser = require('body-parser');
var striptags = require('striptags');
var formidable = require("formidable");
var server = express();
server.use("/public", express.static('public'));
server.use(bodyParser.json());
server.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
// Podpora sejam na strežniku
var expressSession = require('express-session');
server.use(
    expressSession({
        secret: '123456789QWERTY',  // Skrivni ključ za podpisovanje piškotkov
        saveUninitialized: true,    // Novo sejo shranimo
        resave: false,              // Ne zahtevamo ponovnega shranjevanja
        cookie: {
            maxAge: 3600000         // Seja poteče po 1h neaktivnosti
        }
    })
);


// Globalne spremenljivke
// Vsi podatki so shranjeni v teh spremenljivkah, namesto v podatkovni bazi.
var maxMessages = 10;
var rooms = ["Skedenj", "Faks", "Sport", "Razno"];
var messages = {"Skedenj": {"id": 0, "msgs": []}, "Faks": {"id": 0, "msgs": []}, "Sport": {"id": 0, "msgs": []}, "Razno": {"id": 0, "msgs": []}};
messages["Skedenj"]["id"] = 1;
messages["Skedenj"]["msgs"].push({id: 0, user: {id: 42, name: "Marjan Novak"}, time: "2017-06-14 07:30:20", text: "Živijo!"});

// Seznam vseh sob
server.get('/api/rooms', function(request, response) {
    response.send(rooms);
});

// Trenutno prijavljen uporabnik
server.get('/api/user', function(request, response) {
    response.send(request.session.user);
});

// Sporočila za podano sobo :room
server.get('/api/users/:room', function(request, response) {
    var room = request.params.room;
    var users = [];
    var usersSet = {};

    for (var i = 0; i<messages[room].msgs.length; i++) {
        var user = messages[room].msgs[i].user;
        if (!usersSet[user.name]) {
            usersSet[user.name] = true;
            users.push(user);
        }
    }

    response.send(users);
});

// Sporočila od :id naprej za podano sobo :room
server.get('/api/messages/:room/:id', function(request, response) {
    var room = request.params.room;

    var roomMaxMsgId = messages[room].id;
    var msgId = Math.max(0, messages[room].msgs.length - (roomMaxMsgId - parseInt(request.params.id)));

    response.send(messages[room].msgs.slice(msgId));
});

// Pošiljanje sporočila v podano sobo :room
server.post('/api/messages/:room', function (request, response) {
    var room = request.params.room;

    var body = request.body;

    if (body.hasOwnProperty("text") && body.hasOwnProperty("user") &&
        body.user.hasOwnProperty("id") &&  body.user.id === parseInt(body.user.id, 10) &&
        body.user.hasOwnProperty("name") && body.user.name.length <= 50 &&
        body.text.length <= 500) {

        body.text = striptags(body.text);
        body.user.name = striptags(body.user.name);
        body.time = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        body.id = messages[room].id;

        messages[room].id++;
        messages[room].msgs.push(body);

        if (messages[room].msgs.length > maxMessages) {
            messages[room].msgs.shift();
        }

        response.end();
    } else {
        response.status(404);
        response.setHeader("Content-Type", "application/json");
        response.json({"message": "Request malformed."});
    }

});

// Prijava novega uporabnika in hranjenje uporabnika v seji
server.post('/login', function(request, response) {
    usernum = document.getElementById('id');
    username = document.getElementById('name')
    if (usernum >= 0 && usernum <=96 && username.length > 0) {
        request.session.user = username;
        request.session.id = usernum;
    }
    if (request.session.user.length==0) {
        response.redirect('/public/index')
    }
    document.getElementById('username') = request.session.user();
    if (request.session.user.length > 0) {
        response.redirect('/public/login');
    }
});

// Odjava uporabnika
server.post('logout', function(request, response) {
    request.session.length = 0;
    response.redirect('/public/login');
    response.end();
})

server.get('/', function(request, response) {
    //TODO
    response.redirect('/public/login.html');     
})

server.listen(process.env.PORT, function() {
  console.log("Server is running");
});
